# <div align="center">![jupiter-PKGBUILD](images/steam-deck-logo-black.png)![mirror](images/mirror.png)![jupiter-PKGBUILD](images/steam-deck-logo-black-flipped.png)</div>

# Table of Contents

- [Steam Deck (jupiter) PKGBUILD (Source Code) Public Mirror 🪞 (Introduction)](#steam-deck-jupiter-pkgbuild-source-code-public-mirror-)
- [Shallow Clone Recommended for Submodules (Submodule Shallow Clone)](#shallow-clone-recommended-for-submodules-submodule-shallow-clone)
- [Clone Complete Repository (include Submodules)](#clone-complete-repository-include-submodules)
- [Steam Deck (jupiter) PKGBUILD (jupiter-PKGBUILD) Rebase](#steam-deck-jupiter-pkgbuild-jupiter-pkgbuild-rebase)
- 📦 [Build/Make Steam Deck (jupiter) Package](#-buildmake-steam-deck-jupiter-package)
  - 📦 [Automatically](#-automatically)
  - 📦 [Manually](#-manually)
  - ➕ [Additional Software/Packages](#-additional-softwarepackages)
- [SteamOS 3.x (holo) | Steam Deck (jupiter) PKGBUILD (Source Code) Public Mirror 🪞 Portal](#steamos-3x-holo-steam-deck-jupiter-pkgbuild-source-code-public-mirror-portal)
- [Valve (Official) Steam Deck (jupiter) RELEASE BIOS Database](#valve-official-steam-deck-jupiter-release-bios-database)
  - 💾 [Steam Deck (jupiter) BIOS Backup](#-steam-deck-jupiter-bios-backup-) 💽
  - ⚡ [Steam Deck (jupiter) RELEASE BIOS Flash](#-steam-deck-jupiter-release-bios-flash-) ⚡
  - 🧰 [Steam Deck (jupiter) BIOS Tool (jupiter-bios-tool)](#-steam-deck-jupiter-bios-tool-jupiter-bios-tool-) 🧰
  - 🔓 [Steam Deck (jupiter) BIOS Unlock (jupiter-bios-unlock)](#-steam-deck-jupiter-bios-unlock-jupiter-bios-unlock-) 🔓
    - 🧝 [C Binary (jupiter-bios-unlock.c)](#-c-binary-jupiter-bios-unlockc-preferredprimary-) 🧝
    - 🐍 [Python (jupiter-bios-unlock.py)](#-python-jupiter-bios-unlockpy-alternativesecondary-) 🐍
- [Valve (Official) Steam Deck (jupiter) Windows 🪟 Drivers](#valve-official-steam-deck-jupiter-windows-drivers)
- 👍 [Valve (Official) SteamOS 3.x (holo) and Steam Deck (jupiter) Public Repositories](#-valve-official-steamos-3x-holo-and-steam-deck-jupiter-public-repositories-) 👍
- 🙅 [Packages/Repositories Valve, Collabora, and Igalia are in Violation of GNU License](#-packagesrepositories-valve-collabora-and-igalia-are-in-violation-of-gnu-license-) 👎
  - [Manually Deobfuscate Source Package](#manually-deobfuscate-source-package)
  - [GPL Package Research](#gpl-package-research)
- 😉 [GitLab Repository Private-Public How-to](#-gitlab-repository-private-public-how-to-) 😂
- 💭 [Thoughts/Opinions](#-thoughtsopinions-) 💡
- 📜 [License](#-license-) 📜

# <img align="center" alt="jupiter-PKGBUILD" src="images/steam-deck-logo-black.png" width="48"> Steam Deck (jupiter) <img align="center" alt="jupiter-PKGBUILD" src="images/steam-deck-logo-black-flipped.png" width="48"> PKGBUILD (Source Code) Public Mirror 🪞

<div align="center">

***[These humble public repositories (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) (especially the following (elaborate/expansive) [Markdown](https://en.wikipedia.org/wiki/Markdown) documentation) are dedicated to the memory of:***

🫶 ***[Aaron Swartz](https://en.wikipedia.org/wiki/Aaron_Swartz)*** 🫶

</div>

---

Before you ask/assume (*ass-u-me*), no, this is not some [parallel universe](https://en.wikipedia.org/wiki/Multiverse) or [April Fools' Day](https://en.wikipedia.org/wiki/April_Fools'_Day) joke. [These public repositories (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) are an **unmodified 1:1 public copy/mirror** of Valve's **latest** (currently private) [Steam Deck (jupiter) GitLab repositories](https://gitlab.steamos.cloud/jupiter) ([secondary](https://gitlab.steamos.cloud/steam)|[tertiary](https://gitlab.steamos.cloud/Plagman)|[quarternary](https://gitlab.steamos.cloud/misc)|[quinary](https://gitlab.steamos.cloud/endrift)), i.e., **SteamOS 3.x (holo) source code**.

[This repository (jupiter-PKGBUILD)](https://gitlab.com/evlaV/jupiter-PKGBUILD) and [holo-PKGBUILD](https://gitlab.com/evlaV/holo-PKGBUILD) have exclusively (and respectively) been modified (when necessary) to source from this public copy/mirror, i.e., [these public repositories (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) - let's call them **public mirrors**. ***To clarify/elucidate, the ONLY (necessary) modifications made are to specific PKGBUILD - pointing/redirecting to [these public mirrors (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) instead of Valve's (currently private) [Steam Deck (jupiter) GitLab repositories](https://gitlab.steamos.cloud/jupiter) ([secondary](https://gitlab.steamos.cloud/steam)|[tertiary](https://gitlab.steamos.cloud/Plagman)|[quarternary](https://gitlab.steamos.cloud/misc)|[quinary](https://gitlab.steamos.cloud/endrift)).***

These ~~public repositories~~ (*cough*) [**public mirrors** (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) (including [this repository (jupiter-PKGBUILD)](https://gitlab.com/evlaV/jupiter-PKGBUILD)) are automatically maintained via a bot; using a series of bespoke tools (I may open source these tools if there's interest). I started developing these tools on *March 3, 2022*, (after *[Steam Deck (jupiter) Recovery Image 1](https://steamdeck-images.steamos.cloud/recovery/steamdeck-recovery-1.img.bz2)* ([zip](https://steamdeck-images.steamos.cloud/recovery/steamdeck-recovery-1.img.zip)) release date) while reverse engineering the (aforementioned) [Steam Deck (jupiter) Recovery Image](https://steamdeck-images.steamos.cloud/recovery/).

[These **public mirrors** (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) launched *April 1, 2022*, and are automatically maintained (**updated daily**) until Valve officially publicizes their (currently private) [Steam Deck (jupiter) GitLab repositories](https://gitlab.steamos.cloud/jupiter) ([secondary](https://gitlab.steamos.cloud/steam)|[tertiary](https://gitlab.steamos.cloud/Plagman)|[quarternary](https://gitlab.steamos.cloud/misc)|[quinary](https://gitlab.steamos.cloud/endrift)), i.e., **SteamOS 3.x (holo) source code**. All projects are sourced from Valve's latest [official (main) source packages](https://steamdeck-packages.steamos.cloud/archlinux-mirror/sources/).

---

<img align="left" alt="jupiter-PKGBUILD" src="images/steam-deck-logo-black.png" width="48"><img align="right" alt="jupiter-PKGBUILD" src="images/steam-deck-logo-black-flipped.png" width="48"><img align="left" alt="holo-PKGBUILD" src="images/steamos-logo.png" width="48"><img align="right" alt="holo-PKGBUILD" src="images/steamos-logo-flipped.png" width="48">

<div align="center">

***I hope the publication and maintenance of [these public mirrors (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) provide some (much needed) transparency as well as aid, encourage, and inspire SteamOS 3.x / Steam Deck contributors/developers/tinkerers.***

</div>

---

## Shallow Clone Recommended for Submodules (Submodule Shallow Clone)

Due to the (increasingly) redundant nature of the included submodules, a **submodule shallow clone** is recommended to reduce local storage consumption and improve repository performance. A shallow clone will (locally) trim and rebase (graft) repository:

```sh
git clone --recurse-submodules --shallow-submodules https://gitlab.com/evlaV/jupiter-PKGBUILD.git
```

To shallow clone [this repository (jupiter-PKGBUILD)](https://gitlab.com/evlaV/jupiter-PKGBUILD) and submodules:

```sh
git clone --depth=1 --recurse-submodules --shallow-submodules https://gitlab.com/evlaV/jupiter-PKGBUILD.git
```

To shallow clone [this repository (jupiter-PKGBUILD)](https://gitlab.com/evlaV/jupiter-PKGBUILD) and exclude submodules (not recommended):

```sh
git clone --depth=1 https://gitlab.com/evlaV/jupiter-PKGBUILD.git
```

or alternatively:

```sh
git clone --depth 1 https://gitlab.com/evlaV/jupiter-PKGBUILD.git
```

You may consider increasing the depth option parameter/value (**1**) to extend/increase commit history (at the cost of local storage consumption). Set **d**epth **(d=number/integer)** to **30** to retain the **29 (d-1)** latest commits and graft the **30th ((d)th)** commit (variably the commit history of the past month; if repository averages one (1) commit per day). Set depth to **1** for minimum local storage consumption, i.e., the latest (head) commit (grafted); no commit history (recommended).

To (re)shallow existing repository or retain shallow clone depth during/after Git fetch/pull (merge) operations, use option(s) `--depth=n` or `--depth=n --rebase` respectively:

```sh
git fetch --depth=1
git rebase
```

or alternatively:

```sh
git pull --depth=1 --rebase
```

- Optionally prune (potential) loose objects (trim) after (re)shallow:

   ```sh
   git gc --prune=now
   ```

---

To unshallow repository, i.e., restore complete/full clone depth (be prepared for a large download):

```sh
git fetch --unshallow
```

or alternatively:

```sh
git pull --unshallow
```

## Clone Complete Repository (include Submodules)

Use option `--recurse-submodules` during Git clone to include submodules:

```sh
git clone --recurse-submodules https://gitlab.com/evlaV/jupiter-PKGBUILD.git
```

or alternatively:

```sh
git clone https://gitlab.com/evlaV/jupiter-PKGBUILD.git
git submodule update --init --recursive
```

or alternatively:

```sh
git clone https://gitlab.com/evlaV/jupiter-PKGBUILD.git
git submodule init --recursive
git submodule update --recursive
```

---

Use option `--recurse-submodules` during Git fetch/pull (merge) operations to include submodules:

```sh
git fetch --recurse-submodules
```

or alternatively:

```sh
git pull --recurse-submodules
```

## Steam Deck (jupiter) PKGBUILD (jupiter-PKGBUILD) Rebase

[This repository (jupiter-PKGBUILD)](https://gitlab.com/evlaV/jupiter-PKGBUILD) may occasionally be rebased. This will (destructively) remove obsolete commit history and data (to reduce repository size) and improve repository performance, but may disrupt normal Git fetch/pull (merge) operations. If you receive a branch diverged error message upon Git fetch/pull (merge):

```sh
git fetch
git rebase
```

or alternatively:

```sh
git pull --rebase
```

**This will result in (local) commit history and data loss.** *This commit history and data loss is composed/consisting completely of obsolete data which has since been depreciated or replaced by (a) newer/later revision(s).*

> **Latest Rebase: January 11, 2023**

## 📦 Build/Make Steam Deck (jupiter) Package

### 📦 Automatically

```sh
  usage: ./build.sh [package_dirname ...] [option ...] [makepkg_option ...]
example: ./build.sh jupiter-hw-support linux-neptune mesa --srcinfo -f

options:
  --srcinfo         enable (AUR) .SRCINFO generation (before make package)
  --srcinfo-only    ONLY generate (AUR) .SRCINFO (disable/skip make package)

make all (found) package(s) if package (directory name (dirname)) not specified

see makepkg help ('makepkg -h', 'makepkg --help') for makepkg options
```

Run ([included](build.sh)) `./build.sh` to automatically make all (found) or specified pacman packages (PKGBUILD); optionally specify package (directory name (dirname)) as argument(s) to make instead of all (found). Use option `--srcinfo` to enable (AUR) [.SRCINFO](https://wiki.archlinux.org/title/.SRCINFO) generation (before make package) or `--srcinfo-only` to ONLY generate (AUR) [.SRCINFO](https://wiki.archlinux.org/title/.SRCINFO) (disable/skip make package).

Run ([included](build.sh)) `./build.sh -h` for usage instructions.

### 📦 Manually

Change directory to respective pacman package(s) (PKGBUILD) e.g., `cd jupiter-hw-support`

Make respective pacman package (PKGBUILD):

```sh
makepkg
```

Install (potentially) missing dependencies (with pacman) then make respective pacman package (PKGBUILD):

```sh
makepkg -s
```

Install (potentially) missing dependencies (with pacman) then make and install respective pacman package (PKGBUILD):

```sh
makepkg -is
```

Install (potentially) missing dependencies (with pacman) then clean, make, and install respective pacman package (PKGBUILD):

```sh
makepkg -Cis
```

Run `makepkg -h` or see: [makepkg usage](https://wiki.archlinux.org/title/Makepkg#Usage) for detailed usage instructions.

---

### ➕ Additional Software/Packages

- [build.sh](build.sh) | [documentation](#-automatically) | [PKGBUILD](map/PKGBUILD) - make all (found) or specified pacman packages (PKGBUILD)
- [jupiter-bios-tool.py](jupiter-bios-tool.py) | [documentation](#-steam-deck-jupiter-bios-tool-jupiter-bios-tool-) | [PKGBUILD](jupiter-bios-tool/PKGBUILD) - analyze, verify, dump/generate/inject UID, and dynamically trim ✂️ any/all Steam Deck (jupiter) BACKUP and RELEASE BIOS (*_sign.fd)
- **jupiter-bios-unlock**
  - [jupiter-bios-unlock.c](jupiter-bios-unlock.c) | [x86_64 prebuilt C binary](bin/jupiter-bios-unlock) | [documentation](#-c-binary-jupiter-bios-unlockc-preferredprimary-) | [PKGBUILD](jupiter-bios-unlock/PKGBUILD) | [PKGBUILD](jupiter-bios-unlock-bin/PKGBUILD) - enhanced/extended edition (e.g., unlock+lock support) of [SD_Unlocker.c](https://gist.github.com/SmokelessCPUv2/8c1e6559031e199d9a678c9fe2ebf7d4) (preferred/primary)
  - [jupiter-bios-unlock.py](jupiter-bios-unlock.py) | [documentation](#-python-jupiter-bios-unlockpy-alternativesecondary-) | [PKGBUILD](jupiter-bios-unlock-python/PKGBUILD) - Python (wrapper) equivalent (alternative/secondary)

---

## <a href="https://gitlab.com/evlaV/holo-PKGBUILD"><img align="center" alt="holo-PKGBUILD" src="images/steamos-logo.png" width="32"></a> <a href="https://gitlab.com/evlaV/holo-PKGBUILD">SteamOS 3.x (holo)</a> <a href="https://gitlab.com/evlaV/holo-PKGBUILD"><img align="center" alt="holo-PKGBUILD" src="images/steamos-logo-flipped.png" width="32"></a><a href="https://gitlab.com/evlaV/holo-PKGBUILD"><img align="center" alt="portal" src="images/portal-3.png" height="32" width="4"></a><a href="https://gitlab.com/evlaV/jupiter-PKGBUILD"><img align="center" alt="portal" src="images/portal-4.png" height="32" width="4"></a><a href="https://gitlab.com/evlaV/jupiter-PKGBUILD"><img align="center" alt="jupiter-PKGBUILD" src="images/steam-deck-logo-black.png" width="32"></a> <a href="https://gitlab.com/evlaV/jupiter-PKGBUILD">Steam Deck (jupiter)</a> <a href="https://gitlab.com/evlaV/jupiter-PKGBUILD"><img align="center" alt="jupiter-PKGBUILD" src="images/steam-deck-logo-black-flipped.png" width="32"></a> PKGBUILD (Source Code) Public Mirror 🪞 Portal <a href="https://gitlab.com/evlaV/holo-PKGBUILD"><img align="center" alt="portal" src="images/portal-5.png" height="32" width="4"></a><a href="https://gitlab.com/evlaV/jupiter-PKGBUILD"><img align="center" alt="portal" src="images/portal-6.png" height="32" width="4"></a>

<div align="center"><a href="https://gitlab.com/evlaV/holo-PKGBUILD"><img alt="holo-PKGBUILD" src="images/steamos-logo.png" width="128"><img alt="mirror" src="images/mirror.png" height="128"><img alt="holo-PKGBUILD" src="images/steamos-logo-flipped.png" width="128"></a><a href="https://gitlab.com/evlaV/holo-PKGBUILD"><img alt="portal" src="images/portal-1.png" height="128" width="6"></a><a href="https://gitlab.com/evlaV/jupiter-PKGBUILD"><img alt="portal" src="images/portal-2.png" height="128" width="6"></a><a href="https://gitlab.com/evlaV/jupiter-PKGBUILD"><img alt="jupiter-PKGBUILD" src="images/steam-deck-logo-black.png" width="128"><img alt="mirror" src="images/mirror.png" height="128"><img alt="jupiter-PKGBUILD" src="images/steam-deck-logo-black-flipped.png" width="128"></a></div>

---

## Valve (Official) Steam Deck (jupiter) RELEASE BIOS Database

Downloadable database (table) of Valve's official Steam Deck (jupiter) RELEASE BIOS (*_sign.fd) images:

| BIOS Version | EC Revision | Minimum Board Revision | EC Date | BIOS Date | Release Date | Byte Size | BIOS Offset | Notes |
| :--- | :---: | :---: | ---: | ---: | :---: | :---: | :---: | :---: |
| [F7A0119](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/bc5ca4c3fc739d09e766a623efd3d98fac308b3e/usr/share/jupiter_bios/F7A0119_sign.fd) | M11500 | EV3+ | July 26, 2023 | October 24, 2023 | October 26, 2023 | 17778888 | 0xe8c70 |
| [F7A0118](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/f79ccd15f68e915cc02537854c3b37f1a04be9c3/usr/share/jupiter_bios/F7A0118_sign.fd) | M11500 | EV3+ | July 26, 2023 | July 28, 2023 | September 18, 2023 | 17778888 | 0xe8c70 | Actual voltage offset controls. |
| [F7A0116](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/38f7bdc2676421ee11104926609b4cc7a4dbc6a3/usr/share/jupiter_bios/F7A0116_sign.fd) | M11400 | EV3+ | May 4, 2023 | May 12, 2023 | May 16, 2023 | 17778888 | 0xe8c70 | More robust recovery. Charging LED fix in S3/S5. ACPI methods for battery charge level. |
| [F7A0115](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/5644a5692db16b429b09e48e278b484a2d1d4602/usr/share/jupiter_bios/F7A0115_sign.fd) | M11200 | EV3+ | December 19, 2022 | March 17, 2023 | March 27, 2023 | 17778888 | 0xe8c70 |
| [F7A0114](https://deckhd.com/downloads/F7A0114_repack.fd) | M11200 | EV3+ | December 19, 2022 | December 20, 2022 | Never, was preinstalled | 17778888 | 0xe8c70 |
| [F7A0113](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/bf77354719c7a74097a23bed4fb889df4045aec4/usr/share/jupiter_bios/F7A0113_sign.fd) | M11100 | EV3+ | September 12, 2022 | November 4, 2022 | December 9, 2022 | 17778888 | 0xe8c70 |
| [F7A0110](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/0660b2a5a9df3bd97751fe79c55859e3b77aec7d/usr/share/jupiter_bios/F7A0110_sign.fd) | M19000 | EV3+ | August 15, 2022 | August 20, 2022 | August 25, 2022 | 17778888 | 0xe8c70 |
| [F7A0108](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/981c53dc4aadc7a94fed9a5f3fb3832023da1a0b/usr/share/jupiter_bios/F7A0108_sign.fd) | M18000 | EV3+ | July 6, 2022 | July 9, 2022 | July 20, 2022 | 17778888 | 0xe8c70 | 🛑⚠️ **WARNING: Removed EV2 support. Do not apply to EV2 units, it will make the units unbootable.** ⚠️🛑 |
| [F7A0105](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/ae233d982c51b9f9433e6cc6296d99a5b2419f2a/usr/share/jupiter_bios/F7A0105_sign.fd) | T14000 | EV2+ | March 1, 2022 | March 21, 2022 | March 25, 2022 | 17778888 | 0xe8c70 | Added fTPM support, enabling Windows 11 installation. |
| [F7A0104](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/61dcd3d257cb612c4a239a5a8c7eaa878dcceeb3/usr/share/jupiter_bios/F7A0104_sign.fd) | T14000 | EV2+ | March 1, 2022 | March 1, 2022 | March 7, 2022 | 17778888 | 0xe8c70 |
| [~~F7A0103~~](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/adabb79b90e5fc650290d21f696c9853ecd2c3ca/usr/share/jupiter_bios/F7A0103_sign.fd) | T13000 | EV2+ | February 22, 2022 | February 23, 2022 | February 24, 2022 | 17778888 | 0xe8c70 | 🛑⛔ **WARNING: Causes gfx failures!** ⛔🛑 |
| [F7A0102](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/cc85301165aba68bac0d80e96c3b860845a00729/usr/share/jupiter_bios/F7A0102_sign.fd) | T12000 | EV2+ | January 22, 2022 | January 25, 2022 | February 1, 2022 | 17778888 | 0xe8c70 |
| [F7A0101](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/123eb4be5da7db910ddb0be071f08f66724e31fd/usr/share/jupiter_bios/F7A0101_sign.fd) | T11000 | EV2+ | December 20, 2021 | December 21, 2021 | January 13, 2022 | 18385160 | 0x17d050 | Both Micron and Samsung memory supported. |
| [~~F7A0100~~](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/be2508796feb017b2e7eebd006b6a04b3fd9cf2a/usr/share/jupiter_bios/F7A0100_sign.fd) | T10000 | EV2+ | November 15, 2021 | November 16, 2021 | November 16, 2021 | 18385160 | 0x17d050 | 🛑⛔ **WARNING: Samsung memory unsupported! Only Micron (non-Samsung) memory supported!** ⛔🛑 |
| [F7A0033](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/f5ff1849ee033da0ac69f4c6147c3aa66bcecd1a/usr/share/jupiter_bios/F7A0033_sign.fd) | T01140 | EV2+ | November 9, 2021 |  November 10, 2021 | November 15, 2021 | 18385160 | 0x17d050 |
| [F7A0032](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/38982138100fce4bd6454a071a5b88526951de78/usr/share/jupiter_bios/F7A0032_sign.fd) | T01130 | EV2+ | October 29, 2021 | November 1, 2021 | November 3, 2021 | 18385160 | 0x17d050 | Lower power in S3/S5. Some BIOS option refactoring. |
| [F7A0030](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/da3dd55a1ae6a3fa785ad2b566ac53f7538b2bb0/usr/share/jupiter_bios/F7A0030_sign.fd) | T01110 | EV2+ | October 15, 2021 |  October 18, 2021 | October 20, 2021 | 18385160 | 0x17d050 |
| [~~F7A0029~~](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/6b00ae2d03ff64dd6efacd3075b1cfbb044e13a8/usr/share/jupiter_bios/F7A0029_sign.fd) | T01100 | EV2+ | October 4, 2021 | October 5, 2021 | October 6, 2021 | 18385160 | 0x17d050 | 🛑⛔ **WARNING: Maybe bricking units and [we're] not sure the scope of the problem!** ⛔🛑 |
| [~~F7A0028~~](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/e36ba43a27351c0c80b0cf397173b0ab71229c1d/usr/share/jupiter_bios/F7A0028_sign.fd) | T01090 | EV2+ | September 14, 2021 | September 17, 2021 | October 4, 2021 | 18385160 | 0x17d050 | 🛑⛔ **WARNING: Bricks (some?) units with Samsung memory!** ⛔🛑 |
| [F7A0025](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/4d8fb49d1f8fbccc0d5e32dfd797b4ff7123bcfa/usr/share/jupiter_bios/F7A0025_sign.fd) | T01070 | EV2+ | August 20, 2021 | August 24, 2021 | September 2, 2021 | 18385160 | 0x17d050 |
| [F7A0023](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/ab30d2c2d2c3fba1cb8a728f847329d1bf600fe0/usr/share/jupiter_bios/F7A0023_sign.fd) | T01040 | EV2+ | July 29, 2021 | August 6, 2021 | August 10, 2021 | 18385160 | 0x17d050 |
| [F7A0022](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/625799e4b9414e18ad10d72f54f8041835f6bcff/usr/share/jupiter_bios/F7A0022_sign.fd) | T01040 | EV2+ | July 29, 2021 | July 31, 2021  | August 2, 2021 | 18385160 | 0x17d050 |
| [F7A0021](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/a66e14c165e98c54d8a59d491586df4cbe95ea02/usr/share/jupiter_bios/F7A0021_sign.fd) | T01030 | EV2+ | July 6, 2021 | July 6, 2021 | July 30, 2021 | 18385160 | 0x17d050 |
| F7A0019 (UNRELEASED) | T010?0(?) | EV2+ | May 18 - July 30, 2021 (UNRELEASED) | Unknown | May 18 - July 30, 2021 (UNRELEASED) | 18385160(?) | 0x17d050(?) | 🛑⚠️ **WARNING: Removed EV1 support. Do not apply to EV1 units, it will make the units unbootable.** ⚠️🛑 |
| [F7A0016](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/0a04485e8271da9423b6bf963006836e6ccb04b0/usr/share/jupiter_bios/F7A0016_sign.fd) | 0001B0 | EV1+ | May 13, 2021 | May 13, 2021 | May 18, 2021 | 18385160 | 0x17d050 |
| [F7A0015](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/20339362eaeaabd9b939ec9c567d7e634f699a56/usr/share/jupiter_bios/F7A0015_sign.fd) | 0001A0 | EV1+ | April 28, 2021 | May 5, 2021 | May 13, 2021 | 18385160 | 0x17d050 | Fixes the spurious throttling that happens on some boards and gets things to near-final TDP. |
| [F7A0012](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/627985728fd504c43919ca2f9ff5ded801c3db67/usr/share/jupiter_bios/F7A0012_sign.fd) | 000170 | EV1+ | March 31, 2021 | April 1, 2021 | April 1, 2021 | 18385160 | 0x17d050 |
| [F7A0010](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/353e99c0d266e6fde10107f75eff5e8a73510472/usr/share/jupiter_bios/F7A0010_sign.fd) | 000150 | EV1+ | March 19, 2021 | March 22, 2021 | March 23, 2021 | 18385160 | 0x17d050 | Faster boot time after initial memory training. Fix EC failure to read APU temp after warm reboot (resulting in loud fan). ASL and current sensors show up by default now. Fixes for jupiter-fan ACPI interface. |
| [F7A0008](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/c610ed3631c235b9276c9b499565cad07e2cc76f/usr/share/jupiter_bios/F7A0008_sign.fd) | 000130 | EV1+ | March 4, 2021 | March 4, 2021 | March 8, 2021 | 18385160 | 0x17d050 |
| [F7A0007](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/2fc5abc8df3865b8f7f985e9ae262adecd6557c3/usr/share/jupiter_bios/F7A0007_sign.fd) | 000120 | EV1+ | March 1, 2021 | March 1, 2021 | March 1, 2021 | 18385160 | 0x17d050 |
| [F7A0006](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/52d70dbd5bf67ccac77afdbc25868b55a3d765ed/usr/share/jupiter_bios/F7A0006_sign.fd) | 000110 | EV1+ | February 26, 2021 | February 26, 2021 | February 26, 2021 | 18385160 | 0x17d050 | BIOS before F7A0006 requires special steps. |
| [~~Test2~~](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/b4715e2fd8eb480957874221c973a5617db04d8d/usr/share/jupiter_bios/Test2_sign.fd) | 000038 | EV1+ | February 3, 2021 | February 9, 2021 | February 26, 2021 | 18385160 | 0x17d050 | 🛑⛔ **WARNING: Test build!** ⛔🛑 |
| [~~Test1~~](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/b4715e2fd8eb480957874221c973a5617db04d8d/usr/share/jupiter_bios/Test1_sign.fd) | 000038 | EV1+ | February 3, 2021 | February 5, 2021 | February 26, 2021 | 18385112 | 0x17d050 | 🛑⛔ **WARNING: Test build!** ⛔🛑 |

---

### 💾 Steam Deck (jupiter) BIOS Backup 💽

Display current BIOS Version as root (on Steam Deck) - requires `dmidecode`:

```sh
sudo dmidecode -s bios-version
```

**Display current BIOS ID (BIOS Model Name and BIOS Version) as root (on Steam Deck):**

If running official (jupiter) SteamOS (i.e., requires `jupiter-hw-support`):

```sh
sudo /usr/share/jupiter_bios_updater/h2offt -SC
```

or universal; regardless of (if/if not) running official (jupiter) SteamOS (i.e., doesn't require jupiter-hw-support) - requires `curl` and `bsdtar (libarchive)` and optionally (if you download/use zip) `chmod (coreutils)`:

```sh
curl https://gitlab.com/evlaV/jupiter-hw-support/-/archive/master/jupiter-hw-support-master.tar.gz?path=usr/share/jupiter_bios_updater | bsdtar --strip-components=3 -xvf-
# may be required (if you download/use zip)
chmod +x jupiter_bios_updater/h2offt
sudo jupiter_bios_updater/h2offt -SC
```

> `tar.gz` may be substituted for `tar.bz2`, `tar`, or `zip` (zip requires additional `chmod (coreutils)` dependency and command)

---

**Make a Steam Deck (jupiter) BIOS backup as root (on Steam Deck):**

If running official (jupiter) SteamOS (i.e., requires `jupiter-hw-support`):

```sh
# automatically name backup with the current BIOS version - dmidecode or date (coreutils) optional
sudo /usr/share/jupiter_bios_updater/h2offt jupiter-$(sudo dmidecode -s bios-version 2>/dev/null || date +%Y%m%d 2>/dev/null)-bios-backup.bin -O
```

or universal; regardless of (if/if not) running official (jupiter) SteamOS (i.e., doesn't require jupiter-hw-support) - requires `curl` and `bsdtar (libarchive)` and optionally `dmidecode` or `date (coreutils)` and optionally (if you download/use zip) `chmod (coreutils)`:

Download the latest Steam Deck (jupiter) `h2offt` to `./jupiter_bios_updater/` - requires `curl` and `bsdtar (libarchive)`:

```sh
curl https://gitlab.com/evlaV/jupiter-hw-support/-/archive/master/jupiter-hw-support-master.tar.gz?path=usr/share/jupiter_bios_updater | bsdtar --strip-components=3 -xvf-
# may be required (if you download/use zip)
chmod +x jupiter_bios_updater/h2offt
# automatically name backup with the current BIOS version - dmidecode or date (coreutils) optional
sudo jupiter_bios_updater/h2offt jupiter-$(sudo dmidecode -s bios-version 2>/dev/null || date +%Y%m%d 2>/dev/null)-bios-backup.bin -O
```

> `tar.gz` may be substituted for `tar.bz2`, `tar`, or `zip` (zip requires additional `chmod (coreutils)` dependency and command)

> **BACKUP/TRIMMED BIOS byte size: `=16777216` bytes (`0x1000000`) `=128Mb` (`=16MB`)**

---

### ⚡ Steam Deck (jupiter) RELEASE BIOS Flash ⚡

[Steam Deck (jupiter) Recovery Instructions](https://help.steampowered.com/en/faqs/view/1B71-EDF2-EB6D-2BB3)

[Steam Deck (jupiter) Recovery Download Directory](https://steamdeck-images.steamos.cloud/recovery/)

---

**Insyde H2OFFT (Flash Firmware Tool) REQUIRED!** *(download/install (flash) instructions below the following important warning)*

🛑⚠️⛔ **FLASH RESPONSIBLY** ⛔⚠️🛑

🛑⚠️⛔ **Board Revision Final (Latest) Supported BIOS Version IS NOT ARBITRARY! If a later/newer (unsupported) BIOS version (for your respective Board Revision) is flashed, you will DESTROY YOUR HARDWARE! (BIOS Hardware Brick)** ⛔⚠️🛑

**Board Revision 3+ (EV3+) Final (Latest) Supported BIOS Version: [???][????] (PENDING) | SUPPORTED**

> **Board Revision 2 (EV2) Final (Latest) Supported BIOS Version: [F7A0105](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/ae233d982c51b9f9433e6cc6296d99a5b2419f2a/usr/share/jupiter_bios/F7A0105_sign.fd) - F7A0107 (UNRELEASED) | UNSUPPORTED (as of July 20, 2022)**

> **Board Revision 1 (EV1) Final (Latest) Supported BIOS Version: [F7A0016](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/0a04485e8271da9423b6bf963006836e6ccb04b0/usr/share/jupiter_bios/F7A0016_sign.fd) - F7A0018 (UNRELEASED) | UNSUPPORTED (as of May 18 - July 30, 2021)**

🛑⚠️⛔ **Check your Board Revision BEFORE flashing and DO NOT flash later than the Final (Latest) Supported BIOS Version documented above (for your respective Board Revision) or you will DESTROY YOUR HARDWARE! (BIOS Hardware Brick)** ⛔⚠️🛑

ℹ️ ***A BIOS Hardware Brick (BIOS Hard Brick) is a reversible process (especially with a [BIOS Backup](#-steam-deck-jupiter-bios-backup-)), but can also be achieved sans/without (see: [jupiter-bios-tool](#-steam-deck-jupiter-bios-tool-jupiter-bios-tool-)). Disassembly and additional hardware may be required.*** ℹ️

🛎️ **I own a few compatible programmers, including several spare compatible BIOS (Winbond) chips. If anyone has experienced Steam Deck BIOS related hardware failure (BIOS Hard Brick), [I can very likely help you fully restore your hardware (unbrick)](mailto:valve.evlav@proton.me). Up to and including mailing a replacement (pre-programmed) BIOS (Winbond) chip (provided your BIOS (Winbond) chip has failed). I offer this limited service as a courtesy to reduce unnecessary [electronic waste](https://en.wikipedia.org/wiki/Electronic_waste) and/or uneconomical (and unnecessary) [RMA](https://en.wikipedia.org/wiki/Return_merchandise_authorization) to Valve.** 🛎️

---

Display current BIOS Version as root (on Steam Deck) - requires `dmidecode`:

```sh
sudo dmidecode -s bios-version
```

**Display current BIOS ID (BIOS Model Name and BIOS Version) as root (on Steam Deck):**

If running official (jupiter) SteamOS (i.e., requires `jupiter-hw-support`):

```sh
sudo /usr/share/jupiter_bios_updater/h2offt -SC
```

or universal; regardless of (if/if not) running official (jupiter) SteamOS (i.e., doesn't require jupiter-hw-support) - requires `curl` and `bsdtar (libarchive)` and optionally (if you download/use zip) `chmod (coreutils)`:

```sh
curl https://gitlab.com/evlaV/jupiter-hw-support/-/archive/master/jupiter-hw-support-master.tar.gz?path=usr/share/jupiter_bios_updater | bsdtar --strip-components=3 -xvf-
# may be required (if you download/use zip)
chmod +x jupiter_bios_updater/h2offt
sudo jupiter_bios_updater/h2offt -SC
```

> `tar.gz` may be substituted for `tar.bz2`, `tar`, or `zip` (zip requires additional `chmod (coreutils)` dependency and command)

---

1. **Download latest Steam Deck (jupiter) `h2offt`:**

   If running official (jupiter) SteamOS (i.e., requires `jupiter-hw-support`):

   ***You may skip this step (step 1) and proceed to step 2 - `h2offt` is included in `jupiter-hw-support`***

   > ℹ️ remember to replace the following (universal) `h2offt` path `jupiter_bios_updater/h2offt` with `/usr/share/jupiter_bios_updater/h2offt` if running official (jupiter) SteamOS (i.e., requires `jupiter-hw-support`) ℹ️

   or universal; regardless of (if/if not) running official (jupiter) SteamOS (i.e., doesn't require jupiter-hw-support) - requires `curl` and `bsdtar (libarchive)` and optionally (if you download/use zip) `chmod (coreutils)`:

   Download the latest Steam Deck (jupiter) `h2offt` to `./jupiter_bios_updater/` - requires `curl` and `bsdtar (libarchive)`:

   ```sh
   curl https://gitlab.com/evlaV/jupiter-hw-support/-/archive/master/jupiter-hw-support-master.tar.gz?path=usr/share/jupiter_bios_updater | bsdtar --strip-components=3 -xvf-
   # may be required (if you download/use zip)
   chmod +x jupiter_bios_updater/h2offt
   ```

   > `tar.gz` may be substituted for `tar.bz2`, `tar`, or `zip` (zip requires additional `chmod (coreutils)` dependency and command)

   ---

2. **Download latest Steam Deck (jupiter) RELEASE BIOS (*_sign.fd):**

   Download the latest Steam Deck (jupiter) RELEASE BIOS (*_sign.fd) to `./jupiter_bios/` - requires `curl` and `bsdtar (libarchive)`:

   ```sh
   curl https://gitlab.com/evlaV/jupiter-hw-support/-/archive/master/jupiter-hw-support-master.zip?path=usr/share/jupiter_bios | bsdtar --strip-components=3 -xvf-
   ```

   > `zip` may be substituted for `tar.gz`, `tar.bz2`, or `tar`

   ---

3. **Flash (upgrade/downgrade) latest/respective Steam Deck (jupiter) RELEASE BIOS (*_sign.fd) as root (on Steam Deck):**

   If running official (jupiter) SteamOS (i.e., requires `jupiter-hw-support`):

   ```sh
   # show confirm message (-U)
   sudo /usr/share/jupiter_bios_updater/h2offt /usr/share/jupiter_bios/F7A0119_sign.fd -U
   # (if necessary) replace F7A0119 with * (*_sign.fd)
   sudo /usr/share/jupiter_bios_updater/h2offt /usr/share/jupiter_bios/*_sign.fd -U
   ```

   or hastily (risky):

   ```sh
   # no confirm message (hasty/risky); safer to show confirm message (-U)
   sudo /usr/share/jupiter_bios_updater/h2offt /usr/share/jupiter_bios/F7A0119_sign.fd
   # (if necessary) replace F7A0119 with * (*_sign.fd)
   sudo /usr/share/jupiter_bios_updater/h2offt /usr/share/jupiter_bios/*_sign.fd
   ```

   or universal; regardless of (if/if not) running official (jupiter) SteamOS (i.e., doesn't require jupiter-hw-support) - requires universal `h2offt` (**step 1**) i.e., `curl` and `bsdtar (libarchive)`:

   ```sh
   # show confirm message (-U)
   sudo jupiter_bios_updater/h2offt jupiter_bios/F7A0119_sign.fd -U
   # (if necessary) replace F7A0119 with * (*_sign.fd)
   sudo jupiter_bios_updater/h2offt jupiter_bios/*_sign.fd -U
   ```

   or hastily (risky):

   ```sh
   # no confirm message (hasty/risky); safer to show confirm message (-U)
   sudo jupiter_bios_updater/h2offt jupiter_bios/F7A0119_sign.fd
   # (if necessary) replace F7A0119 with * (*_sign.fd)
   sudo jupiter_bios_updater/h2offt jupiter_bios/*_sign.fd
   ```

---

### 🧰 Steam Deck (jupiter) BIOS Tool (jupiter-bios-tool) 🧰

Run ([included](jupiter-bios-tool.py)) `./jupiter-bios-tool.py` to **automatically analyze, verify, dump/generate/inject UID,** and **dynamically trim** ✂️ any/all [Steam Deck (jupiter) BACKUP and RELEASE BIOS (*_sign.fd)](#valve-official-steam-deck-jupiter-release-bios-database) (`>0x1000000`) images to `0x1000000` for hardware flashing/programming to a [Winbond W25Q128JW 128Mb (16MB) Serial NOR Flash](https://www.winbond.com/hq/product/code-storage-flash-memory/serial-nor-flash/?__locale=en&partNo=W25Q128JW) ([iFixit](https://www.ifixit.com/Guide/Steam+Deck+Chip+ID/147811)).

**jupiter-bios-tool** is nondestructive, i.e., only **reads** `SOURCE` (**never writes**) and **never overwrites** `DESTINATION` (must be new/nonexistent).

```sh
usage: jupiter-bios-tool.py [SOURCE_BIOS_IMAGE[.bin|.fd|.rom]]
       [DESTINATION_BIOS_IMAGE[.bin|.rom]] [-h] [-d] [-g] [-i] [-r]

 e.g.: jupiter-bios-tool.py jupiter-F7A0118-bios-backup.bin -d
       jupiter-bios-tool.py F7A0118_sign.fd F7A0118-trimmed.bin -i

positional arguments:
  SOURCE_BIOS_IMAGE[.bin|.fd|.rom]
                        analyze/verify SOURCE BIOS image (e.g., F7A0118_sign.fd)
  DESTINATION_BIOS_IMAGE[.bin|.rom]
                        dynamically trim SOURCE BIOS image and/or inject UID
                        to DESTINATION (SOURCE -> DESTINATION)

options:
  -h, --help            show this help message and exit
  -d [DUMP_UID_TO_FILE], --dump-uid [DUMP_UID_TO_FILE]
                        dump SOURCE UID to SPECIFIED file
                        (default: dumped_jupiter-UID.bin)
  -g [GENERATE_UID_TO_FILE], --generate-uid [GENERATE_UID_TO_FILE]
                        generate (pseudorandom) UID to SPECIFIED file
                        (default: generated_jupiter-UID.bin) and inject to
                        DESTINATION (if SPECIFIED)
  -i [INJECT_UID_FROM_FILE], --inject-uid [INJECT_UID_FROM_FILE]
                        inject UID from SPECIFIED file
                        (default: dumped_jupiter-UID.bin) to DESTINATION
  -r, --remove-uid      remove UID from SOURCE to DESTINATION
                        (commonize/sanitize/shareable)
```

| Minimum Python Version | Recommended Python Version |
| :---: | :---: |
| **Python 2.6+/3.1+** (requires backported module: `argparse`) | **Python 2.7+/3.2+** |

To analyze/verify BIOS image info:

`jupiter-bios-tool.py [SOURCE_BIOS_IMAGE[.bin|.fd|.rom]]`

```sh
./jupiter-bios-tool.py F7A0118_sign.fd
```

`-d, --dump-uid` to dump `SOURCE` UID to SPECIFIED file (defaults to `dumped_jupiter-UID.bin`):

`jupiter-bios-tool.py [SOURCE_BIOS_IMAGE[.bin|.fd|.rom]] -d [DUMP_UID_TO_FILE]`

```sh
./jupiter-bios-tool.py jupiter-F7A0118-bios-backup.bin -d
```

`-g, --generate-uid` to generate (pseudorandom) UID to SPECIFIED file (defaults to `generated_jupiter-UID.bin`):

`jupiter-bios-tool.py -g [GENERATE_UID_TO_FILE]`

```sh
./jupiter-bios-tool.py -g
```

> `-g, --generate-uid` additionally injects generated UID to `DESTINATION` (if SPECIFIED) - unless a different/nondefault UID file is SPECIFIED with `-i, --inject-uid` option

`-i, --inject-uid` to inject UID from SPECIFIED file (defaults to `dumped_jupiter-UID.bin`) to `DESTINATION`:

`jupiter-bios-tool.py [SOURCE_BIOS_IMAGE[.bin|.fd|.rom]] [DESTINATION_BIOS_IMAGE[.bin|.rom]] -i [INJECT_UID_FROM_FILE]`

```sh
./jupiter-bios-tool.py F7A0118_sign.fd jupiter-F7A0118-injected-bios.bin -i
```

Trimming is automatically and dynamically detected and performed; must SPECIFY both `SOURCE` and `DESTINATION`:

`jupiter-bios-tool.py [SOURCE_BIOS_IMAGE[.bin|.fd|.rom]] [DESTINATION_BIOS_IMAGE[.bin|.rom]]`

```sh
./jupiter-bios-tool.py F7A0118_sign.fd jupiter-F7A0118-trimmed-bios.bin
```

> **TRIMMED/BACKUP BIOS byte size: `=16777216` bytes (`0x1000000`) `=128Mb` (`=16MB`)**

> **UNTRIMMED RELEASE BIOS byte size: `>16777216` bytes (`>0x1000000`) `>128Mb` (`>16MB`)**

---

⚠️ **compare/`diff` trimmed BIOS with (matching) BIOS backup before hardware flashing/programming.** ⚠️

⚡ **ALWAYS FLASH/PROGRAM AT 1.8V - DO NOT USE 3.3V or 5V (make/use a 1.8V adapter/regulator if your programmer doesn't support 1.8V)** ⚡

I delayed the release of [this tool (~~jupiter-bios-trim~~ jupiter-bios-tool)](jupiter-bios-tool.py) due to the following (formerly missing) features I hoped/planned to complete prior (better late than never):

- [x] detect and print BIOS image version string (untrimmed: 0x790c7e, trimmed: 0x6a800e) - size: 7 bytes (0x7) | negative offset (-0x19 (-25 bytes)) from "CHACHANI-VANGOGH" - block/string is duplicated/repeated at +0x800000 (untrimmed: 0xf90c7e, trimmed: 0xea800e)
- [x] detect, print, dump, generate (pseudorandom), and inject (restore) UID (untrimmed: 0x78cc70, trimmed: 0x6a4000) - byte size: ~372 bytes (0x174) - **jupiter-bios-trim -> jupiter-bios-tool**

---

### 🔓 Steam Deck (jupiter) BIOS Unlock (jupiter-bios-unlock) 🔓

> **Final (Latest) Supported BIOS Version: [F7A0116](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/38f7bdc2676421ee11104926609b4cc7a4dbc6a3/usr/share/jupiter_bios/F7A0116_sign.fd) - F7A0117 (UNRELEASED) | UNSUPPORTED (as of September 18, 2023)**

***Supports Steam Deck (jupiter) BIOS Versions prior to / less-than F7A0118 (<F7A0118) i.e., less-than or equal to (<=F7A0116/F7A0117)***

> **Unsupported BIOS Version: [F7A0118](https://gitlab.com/evlaV/jupiter-hw-support/-/raw/f79ccd15f68e915cc02537854c3b37f1a04be9c3/usr/share/jupiter_bios/F7A0118_sign.fd)+ (as of September 18, 2023)**

---

### 🧝 C Binary (jupiter-bios-unlock.c) | preferred/primary 🧝

([included](jupiter-bios-unlock.c)) `./jupiter-bios-unlock.c` ([included x86_64 prebuilt C binary](bin/jupiter-bios-unlock) | [glibc 2.31+](https://sourceware.org/glibc/wiki/Glibc%20Timeline)) is an enhanced/extended edition (e.g., unlock+lock support) of [SD_Unlocker.c](https://gist.github.com/SmokelessCPUv2/8c1e6559031e199d9a678c9fe2ebf7d4) (thank you [@SmokelessCPUv2](https://github.com/SmokelessCPUv2)).

```sh
usage: [sudo] ./jupiter-bios-unlock [-l] [--lock]

options:
  -l, --lock  🔒 lock BIOS (reverse unlock) 🔒
```

- Optionally compile C binary yourself - not possible if running official (jupiter) SteamOS:

   ```sh
   # compile with gcc
   gcc -O1 jupiter-bios-unlock.c -o jupiter-bios-unlock
   # compile with clang
   clang -O1 jupiter-bios-unlock.c -o jupiter-bios-unlock
   ```

- or optionally download [included x86_64 prebuilt C binary](bin/jupiter-bios-unlock) | [glibc 2.31+](https://sourceware.org/glibc/wiki/Glibc%20Timeline):

   ```sh
   curl -O https://gitlab.com/evlaV/jupiter-PKGBUILD/-/raw/master/bin/jupiter-bios-unlock
   chmod +x jupiter-bios-unlock
   ```

Run optionally compiled C binary or optionally downloaded or ([included](bin/jupiter-bios-unlock)) `./bin/jupiter-bios-unlock` ([included x86_64 prebuilt C binary](bin/jupiter-bios-unlock) | [glibc 2.31+](https://sourceware.org/glibc/wiki/Glibc%20Timeline)) as root (on Steam Deck):

```sh
# compiled C binary or downloaded prebuilt C binary
sudo ./jupiter-bios-unlock
# included prebuilt C binary
sudo ./bin/jupiter-bios-unlock
```

`Steam Deck (jupiter) BIOS successfully 🔓 unlocked 🔓 (AMD CBS/PBS).`

---

🔒 Lock BIOS (reverse unlock) 🔒 as root (on Steam Deck):

```sh
# compiled C binary or downloaded prebuilt C binary
sudo ./jupiter-bios-unlock -l
# included prebuilt C binary
sudo ./bin/jupiter-bios-unlock -l
```

or alternatively:

```sh
# compiled C binary or downloaded prebuilt C binary
sudo ./jupiter-bios-unlock --lock
# included prebuilt C binary
sudo ./bin/jupiter-bios-unlock --lock
```

`Steam Deck (jupiter) BIOS successfully 🔒 locked 🔒 (AMD CBS/PBS).`

### 🐍 Python (jupiter-bios-unlock.py) | alternative/secondary 🐍

([included](jupiter-bios-unlock.py)) `./jupiter-bios-unlock.py` is a Python (wrapper) equivalent of ([included](jupiter-bios-unlock.c)) `./jupiter-bios-unlock.c`

```sh
usage: [sudo] jupiter-bios-unlock.py [-h] [-l] [--help] [--lock]

options:
  -h, --help  show this help message and exit
  -l, --lock  🔒 lock BIOS (reverse unlock) 🔒
```

- Minimum Python Version:
  - **Python 2.3+** (requires module: `portio`) - `portio` requires **Python >= 2.3**
- Recommended Python Version:
  - **Python 3.0+** (requires module: `portio`) - package: `python-portio` / `python3-portio`

#### Pip Install Python module `portio` (dependency) - not possible if running official (jupiter) SteamOS

If not running official (jupiter) SteamOS:

```sh
pip install portio
```

or alternatively:

```sh
pip install -r ./jupiter-bios-unlock-python/requirements.txt
```

Run as root (on Steam Deck):

```sh
sudo ./jupiter-bios-unlock.py
```

`Steam Deck (jupiter) BIOS successfully 🔓 unlocked 🔓 (AMD CBS/PBS).`

---

🔒 Lock BIOS (reverse unlock) 🔒 as root (on Steam Deck):

```sh
sudo ./jupiter-bios-unlock.py -l
```

or alternatively:

```sh
sudo ./jupiter-bios-unlock.py --lock
```

`Steam Deck (jupiter) BIOS successfully 🔒 locked 🔒 (AMD CBS/PBS).`

#### Package Install Python module `portio` (dependency) - not possible if running official (jupiter) SteamOS

If not running official (jupiter) SteamOS:

**Arch (AUR):**

```sh
# install (AUR) package with paru
paru -S python-portio
# install (AUR) package with yay
yay -S python-portio
```

**Debian/Ubuntu:**

```sh
# install package with apt
sudo apt install python3-portio
# install package with apt-get
sudo apt-get install python3-portio
```

Run as root (on Steam Deck):

```sh
sudo ./jupiter-bios-unlock.py
```

`Steam Deck (jupiter) BIOS successfully 🔓 unlocked 🔓 (AMD CBS/PBS).`

---

🔒 Lock BIOS (reverse unlock) 🔒 as root (on Steam Deck):

```sh
sudo ./jupiter-bios-unlock.py -l
```

or alternatively:

```sh
sudo ./jupiter-bios-unlock.py --lock
```

`Steam Deck (jupiter) BIOS successfully 🔒 locked 🔒 (AMD CBS/PBS).`

---

`./jupiter-bios-unlock.py` can be reduced to `4` lines 😉:

```python
import portio
portio.ioperm(0x72, 2, 1)
portio.outb(0xf7, 0x72)
portio.outb(0x77, 0x73)
```

Steam Deck (jupiter) BIOS lock (reverse unlock) can also be reduced to `4` lines 😉:

```python
import portio
portio.ioperm(0x72, 2, 1)
portio.outb(0xf7, 0x72)
portio.outb(0x00, 0x73)
```

`./jupiter-bios-unlock.c` and `./jupiter-bios-unlock.py` are both ~~`24` lines (unlock only)~~ `32` lines (unlock+lock) 😲

---

An amazing (related) resource:

[Unlock the Steam Deck BIOS to UnderVolt and Overclock](https://www.stanto.com/games/unlocking-the-steam-deck-bios-for-under-voltage-and-over-clocking/)

---

## Valve (Official) Steam Deck (jupiter) Windows 🪟 Drivers

[Steam Deck (jupiter) Windows 🪟 Resources (Drivers) Download Directory](https://steamdeck-packages.steamos.cloud/misc/windows/drivers/)

[Steam Deck (jupiter) Windows 🪟 Resources (Drivers) Website](https://help.steampowered.com/en/faqs/view/6121-ECCD-D643-BAA8)

Downloadable table of Valve's official Windows 🪟 Drivers:

| Driver | Release/Update Date | Installation Instructions |
| :--- | ---: | :---: |
| **[APU](https://steamdeck-packages.steamos.cloud/misc/windows/drivers/Aerith%20Windows%20Driver_2302270303.zip)** | **March 21, 2023** | run setup.exe |
| [Wi-Fi](https://steamdeck-packages.steamos.cloud/misc/windows/drivers/RTLWlanE_WindowsDriver_2024.0.10.137_Drv_3.00.0039_Win11.L.zip) | March 10, 2022 | run install.bat |
| [Bluetooth](https://steamdeck-packages.steamos.cloud/misc/windows/drivers/RTBlueR_FilterDriver_1041.3005_1201.2021_new_L.zip) | March 10, 2022 | run installdriver.cmd |
| [SD Card reader](https://steamdeck-packages.steamos.cloud/misc/windows/drivers/BayHub_SD_STOR_installV3.4.01.89_W10W11_logoed_20220228.zip) | March 10, 2022 | run setup.exe |
| **[Audio 1](https://steamdeck-packages.steamos.cloud/misc/windows/drivers/cs35l41-V1.2.1.0.zip)** | **October 10, 2022** | right click `cs35l41.inf` then left click `Install` option |
| [Audio 2](https://steamdeck-packages.steamos.cloud/misc/windows/drivers/NAU88L21_x64_1.0.6.0_WHQL%20-%20DUA_BIQ_WHQL.zip) | March 10, 2022 | right click `NAU88L21.inf` then left click `Install` option |

> ***Windows 11: right click .inf then left click `Show more options` option to enable/unhide `Install` option***

---

## 👍 Valve (Official) SteamOS 3.x (holo) and Steam Deck (jupiter) Public Repositories 👍

(Pitiful) Table of Valve's officially public SteamOS 3.x (holo) and Steam Deck (jupiter) repositories:

| Repository | Branch | Release Date |
| :--- | :---: | ---: |
| [wireplumber](https://gitlab.steamos.cloud/jupiter/wireplumber) | jupiter | June 30, 2022 |
| [steamos-devkit-service](https://gitlab.steamos.cloud/devkit/steamos-devkit-service) | holo* / jupiter | March 30, 2022 |
| [steamos-devkit-client](https://gitlab.steamos.cloud/devkit/steamos-devkit) | - | March 9, 2022 |
| [steam-im-modules](https://github.com/valve-project/steam-qt-keyboard-plugin) | holo* / jupiter | February 23, 2022 |
| [gamescope](https://github.com/Plagman/gamescope) | holo* / jupiter | predates SteamOS 3.x / Steam Deck |

***Table contains ONLY software developed (or modified) by Valve, i.e., excludes unmodified public software.***

> *Denotes no respective PKGBUILD currently exists (i.e., no dedicated build/package).

---

## 🙅 Packages/Repositories Valve, Collabora, and Igalia are in Violation of GNU License 👎

> [There are hundreds of missing GPL packages' source code](#gpl-package-research) - notable examples: `pacman` and `pacman-mirrorlist` - **I believe the latter may have been intentionally omitted to abscond Valve's source package location ([smoking gun](https://en.wikipedia.org/wiki/Smoking_gun) perhaps?)**

[GPL Violators](https://www.gnu.org/licenses/gpl-violation.html) (e.g., [**Valve**](https://www.valvesoftware.com/en/about), [**Collabora**](https://www.collabora.com/about-us/), and [**Igalia**](https://www.igalia.com/about/)) are those who **distribute products based on GPL software without complying with the license conditions**; such as **providing the *complete* source code**, a copy of the license, and **a (written) offer for source code**. **GPL violations are considered a form of infringing use of the software and can be legally pursued by the copyright holders.**

These [GPL Violators](https://www.gnu.org/licenses/gpl-violation.html) (e.g., [**Valve**](https://www.valvesoftware.com/en/about), [**Collabora**](https://www.collabora.com/about-us/), and [**Igalia**](https://www.igalia.com/about/)) are unfair and disrespectful to the free software community and the authors of the respective software. They are taking advantage of the benefits of GPL software without honoring the obligations and the spirit of the license. They deprive users of their freedom to study, modify, and share the software. They should be educated about the meaning and importance of GPL and other free licenses, and be encouraged to adopt ethical/sound practices for the incorporation of GPL components into their products. They should also be held accountable for their actions and be required to remedy their violations by complying with the license terms or ceasing distribution of the infringing products. They should additionally be deterred from continuing and/or repeating violations via public awareness campaigns and legal actions. **I'm endeavoring on both fronts (public awareness and legal action) to right (and *write*) these wrongs.**

[**Valve**](https://www.valvesoftware.com/en/about), [**Collabora**](https://www.collabora.com/about-us/), and [**Igalia**](https://www.igalia.com/about/) should respect the GPL and comply with its terms as soon as possible (it's been over **18 months** now). They should also communicate clearly and transparently about their use of GPL software and their "plans" to adequately release the source code. They should also collaborate with the free software community and contribute to the development and improvement of GPL software. A good/healthy start would be (finally) opening dialogue with me. In case you hadn't noticed, I'm quite proficient and have been **doing your job** for over **18 months** now. It's about time we had a **real discussion**. I would greatly appreciate if someone from [**Valve**](https://www.valvesoftware.com/en/about), [**Collabora**](https://www.collabora.com/about-us/), and/or [**Igalia**](https://www.igalia.com/about/) would (finally) respond to me and address these grievances.

A very bad trend has been set and the status quo (creatively) continued regarding 🄯 [copyleft](https://en.wikipedia.org/wiki/Copyleft). Violations are frequent, and violators are infrequently held accountable. I begrudgingly feel copyleft is seen as an [honor system](https://en.wikipedia.org/wiki/Honor_system), and you should (realistically) presume all your copyleft projects and contributions will be interpreted/used with a [permissive license](https://en.wikipedia.org/wiki/Permissive_software_license). So you might as well pick a "good/decent" permissive (or [middle ground](https://www.mozilla.org/en-US/MPL/2.0/FAQ/)) license (since that's unfortunately the (capitalistic/corporate) world we live in) or [release into the public domain](https://unlicense.org/). I've personally demonstrated both with this project ([LICENSE](LICENSE), [UNLICENSE](UNLICENSE), and [0BSD](LICENSE.0BSD)).

I've privately informed (and now publicly informed and condemned) both [**Collabora**](https://www.collabora.com/about-us/) and [**Igalia**](https://www.igalia.com/about/) of the following (neither have responded):

`"There's a conflict of interest working and/or consulting with Valve on this particular project (SteamOS 3.x / Steam Deck)."`

**(March 25, 2023):** The [FSF](https://www.fsf.org/about/) (`rms`) has demonstrated the following to me (in the case of Valve) regarding [defending their GPL](https://www.gnu.org/licenses/):

[rms](https://en.wikipedia.org/wiki/Richard_Stallman):

`"The FSF is short-handed in the legal area. Given how little it could hope to achieve, I tend to think that this would not be an efficient choice of priorities (though I'm not the one who decides nowadays). I don't think I could get a staff person to verify the origin of the copied code in less than months."`

This interaction has (sadly) not instilled much faith in the FSF (or the GPL) and only further solidifies my license recommendations above. Regardless, thank you `rms`, for your time, honesty/transparency, inquiries/interest, and recommendation. You've been the most communicative and inquisitive person I've interacted with regarding this. I'm sorry our discussions were centered around this deplorable injustice.

I'm presently moving forward with the [Software Freedom Conservancy](https://sfconservancy.org/about/) (with `rms`'s recommendation/referral). **PROGRESS PENDING**

**Come along with me as I foolishly try and take the pulse of a piece of the corporate software/technology world in 2023 ... It will be fun, they said.**

I'm well aware that many people don't technically fully comprehend/understand the GPL. This is a far bigger (systemic) issue than simply Valve, Collabora, and Igalia. I'd argue most people are ignorant and/or misinformed regarding the GPL. I **highly recommend** ALL Linux/OSS users/enthusiasts, especially if you work for a company which may utilize (and also potentially violate) GPL software, **THOROUGHLY READ THE GPL**. The following is pretty well documented (shocker) in the GPL (if only people could be bothered enough to actually read it).

The GPL (GPL2, GPL3, AGPL, and LGPL3) stipulates that sources must accompany binaries, otherwise a (written) offer for source code (or equivalent stipulated offer) is required. See GPL2 Section 3, or GPL3 Section 6 for accepted methods of distributing sources. If one or more of these methods is not followed by Valve, then a GPL violation has occurred. I presently count [~~dozens~~](#third-party-packagesrepositories-valve-collabora-and-igalia-are-in-violation-of-gnu-license) [hundreds](#gpl-package-research) of GPL violations which have been unresolved for over **18 months** now.

See: [GPL FAQ](https://www.gnu.org/licenses/gpl-faq.html#WhatDoesWrittenOfferValid)

*"If you choose to provide source through a written offer, then anybody who requests the source from you is entitled to receive it."*

*"If you commercially distribute binaries not accompanied with source code, the GPL says you must provide a written offer to distribute the source code later."*

Thank you for taking the time to learn a bit about proper copyleft (GPL) software distribution. I hope you've learned not just to comprehend/understand the GPL, but to also value and respect the free/libre license. Could the community do us all a favor and help inform Valve, Collabora, and Igalia of this fact?

But don't take my word for it, the following is a excerpt from my aforementioned discussion with `rms`:

`"Selling the computer is distribution of the preinstalled binaries of GPL-covered programs. GPL 2 and GPL 3 (and AGPL also, and LGPL 3) all require making the source available along with the binaries.`

`[Section] 6 of GPL 3 states the accepted ways of making the source available.`

`If Valve does not do any of those ways, it is violating the GPL."`

---

> **SteamOS 3.x (holo) / Steam Deck (jupiter) Release Date: [February 25, 2022](https://www.timeanddate.com/date/durationresult.html?m1=2&d1=25&y1=2022&ti=on)**
>
> [**Number of Days Valve, Collabora, and Igalia are in Violation of GNU License (i.e., since Steam Deck release)**](https://www.timeanddate.com/date/durationresult.html?m1=2&d1=25&y1=2022&ti=on)

**"If it looks like a duck, swims like a duck, and quacks like a duck, then it probably is a duck."**

<img alt="the cake is a lie." src="images/the-cake-is-a-lie.jpg" width="512">

**the cake is a lie.**

**the cake is a lie.**

**the cake is a lie.**

**the cake is a lie.**

***But not this cake! Happy Cake Day Steam Deck! Thank you Valve for showing the world GNU/Linux is the true home of PC gaming! Please support [free software](https://www.gnu.org/software/). (February 25, 2023)***

<img alt="the cake is not a lie." src="images/the-cake-is-not-a-lie.jpg" width="512">

---

Table of packages/repositories **Valve, Collabora, and Igalia are in Violation of respective GNU License(s):**

### Third Party Packages/Repositories Valve, Collabora, and Igalia are in Violation of GNU License

| Package/Repository | Branch | License | Reason |
| :--- | :---: | ---: | :---: |
| [archlinux-keyring](https://gitlab.archlinux.org/archlinux/archlinux-keyring) | holo, jupiter | [GPLv3](https://gitlab.archlinux.org/archlinux/archlinux-keyring/-/blob/master/LICENSE) | **obfuscated** source code* |
| [bluez](https://git.kernel.org/pub/scm/bluetooth/bluez.git) | holo, jupiter | [GPLv2](https://git.kernel.org/pub/scm/bluetooth/bluez.git/tree/COPYING) | **obfuscated** source code* |
| [bmap-tools](https://github.com/intel/bmap-tools) | holo | [GPLv2](https://github.com/intel/bmap-tools/blob/master/LICENSE) | **obfuscated** source code* |
| [calamares](https://github.com/calamares/calamares) | holo | [GPLv3](https://github.com/calamares/calamares/blob/calamares/LICENSES/GPL-3.0-or-later.txt), [LGPLv3](https://github.com/calamares/calamares/blob/calamares/LICENSES/LGPL-3.0-or-later.txt), [LGPLv2.1](https://github.com/calamares/calamares/blob/calamares/LICENSES/LGPL-2.1-only.txt) | **obfuscated** source code* |
| [casync](https://github.com/systemd/casync) | holo | [LGPLv2.1](https://github.com/systemd/casync/blob/main/LICENSE.LGPL2.1) | **obfuscated** source code* |
| [ckbcomp](https://salsa.debian.org/installer-team/console-setup) | holo | [GPLv2](https://salsa.debian.org/installer-team/console-setup/-/blob/master/GPL-2) | **obfuscated** source code* |
| [dkms](https://github.com/dell/dkms) | holo | [GPLv2](https://github.com/dell/dkms/blob/master/COPYING) | **obfuscated** source code* |
| [f3](https://github.com/AltraMayor/f3) | jupiter | [GPLv3](https://github.com/AltraMayor/f3/blob/master/LICENSE) | **obfuscated** source code* |
| [ffmpeg](https://git.ffmpeg.org/gitweb/ffmpeg) | holo | [GPLv3](https://git.ffmpeg.org/gitweb/ffmpeg.git/blob/HEAD:/COPYING.GPLv3), [GPLv2](https://git.ffmpeg.org/gitweb/ffmpeg.git/blob/HEAD:/COPYING.GPLv2), [LGPLv3](https://git.ffmpeg.org/gitweb/ffmpeg.git/blob/HEAD:/COPYING.LGPLv3), [LGPLv2.1](https://git.ffmpeg.org/gitweb/ffmpeg.git/blob/HEAD:/COPYING.LGPLv2.1) | **obfuscated** source code* |
| [flatpak](https://github.com/flatpak/flatpak) | jupiter | [LGPLv2.1](https://github.com/flatpak/flatpak/blob/main/COPYING) | **obfuscated** source code* |
| [grub](https://www.gnu.org/software/grub/) | holo, jupiter | [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt) | **obfuscated** source code* |
| [holo-keyring](https://gitlab.com/evlaV/holo-keyring) (fork of [archlinux-keyring](https://gitlab.archlinux.org/archlinux/archlinux-keyring)) | holo | [GPLv3](https://gitlab.com/evlaV/holo-keyring/-/blob/master/LICENSE) | **obfuscated** source code* |
| [ibus-anthy](https://github.com/ibus/ibus-anthy) | jupiter | [GPLv2](https://github.com/ibus/ibus-anthy/blob/main/COPYING) | **obfuscated** source code* |
| [ibus-table-cangjie-lite](https://github.com/mike-fabian/ibus-table-chinese) | jupiter | [GPLv3](https://github.com/mike-fabian/ibus-table-chinese/blob/main/COPYING) | **obfuscated** source code* |
| [iwd](https://git.kernel.org/pub/scm/network/wireless/iwd.git) | holo | [LGPLv2.1](https://git.kernel.org/pub/scm/network/wireless/iwd.git/tree/COPYING) | **obfuscated** source code* |
| [kscreen](https://invent.kde.org/plasma/kscreen) | jupiter | [GPLv3](https://invent.kde.org/plasma/kscreen/-/blob/master/LICENSES/GPL-3.0-only.txt), [GPLv2](https://invent.kde.org/plasma/kscreen/-/blob/master/LICENSES/GPL-2.0-or-later.txt), [LGPLv2](https://invent.kde.org/plasma/kscreen/-/blob/master/LICENSES/LGPL-2.0-or-later.txt) | **obfuscated** source code* |
| [kmod](https://git.kernel.org/pub/scm/utils/kernel/kmod/kmod.git) | holo | [LGPLv2.1](https://git.kernel.org/pub/scm/utils/kernel/kmod/kmod.git/tree/COPYING) | **obfuscated** source code* |
| [kupdate-notifier](https://gitlab.com/evlaV/kupdate-notifier) | holo | [LGPLv2.1](https://gitlab.com/evlaV/kupdate-notifier/-/blob/master/LICENSE) | **obfuscated** source code* |
| [lib32-alsa-lib](https://github.com/alsa-project/alsa-lib) | jupiter | [LGPLv2.1](https://github.com/alsa-project/alsa-lib/blob/master/COPYING) | **obfuscated** source code* |
| [lib32-libnm](https://gitlab.freedesktop.org/NetworkManager/NetworkManager) | jupiter | [GPLv3](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/main/COPYING), [LGPLv2.1](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/main/COPYING.LGPL) | **obfuscated** source code* |
| [libevdi](https://github.com/DisplayLink/evdi) | jupiter | [GPLv2](https://github.com/DisplayLink/evdi/blob/devel/module/LICENSE), [LGPLv2.1](https://github.com/DisplayLink/evdi/blob/devel/library/LICENSE) | **obfuscated** source code* |
| [libxcrypt-compat](https://github.com/besser82/libxcrypt) | holo | [LGPLv2.1](https://github.com/besser82/libxcrypt/blob/develop/COPYING.LIB) | **obfuscated** source code* |
| [linux](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git) | holo | [GPLv2](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/LICENSES/preferred/GPL-2.0) | **obfuscated** source code* |
| [linux-firmware-neptune](https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git) | jupiter | [GPLv3](https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/tree/GPL-3), [GPLv2](https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/tree/GPL-2) | **obfuscated** source code* |
| [linux-lts](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git) | holo | [GPLv2](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/LICENSES/preferred/GPL-2.0) | **obfuscated** source code* |
| [linux-neptune-61](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git) | jupiter | [GPLv2](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/LICENSES/preferred/GPL-2.0) | **obfuscated** source code* |
| [linux-neptune-60](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git) | jupiter | [GPLv2](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/LICENSES/preferred/GPL-2.0) | **obfuscated** source code* |
| [linux-neptune](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git) | jupiter | [GPLv2](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/LICENSES/preferred/GPL-2.0) | **obfuscated** source code* |
| [linux-neptune-rtw](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git) | jupiter | [GPLv2](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/LICENSES/preferred/GPL-2.0) | **obfuscated** source code* |
| [makedumpfile](https://github.com/makedumpfile/makedumpfile) | holo | [GPLv2](https://github.com/makedumpfile/makedumpfile/blob/master/COPYING) | **obfuscated** source code* |
| [networkmanager](https://gitlab.freedesktop.org/NetworkManager/NetworkManager) | jupiter | [GPLv3](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/main/COPYING), [LGPLv2.1](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/main/COPYING.LGPL) | **obfuscated** source code* |
| [noise-suppression-for-voice](https://github.com/werman/noise-suppression-for-voice) | holo | [GPLv3](https://github.com/werman/noise-suppression-for-voice/blob/master/LICENSE) | **obfuscated** source code* |
| [pacman-system-update](https://github.com/gportay/pacman-system-update) | holo | [LGPLv2.1](https://github.com/gportay/pacman-system-update/blob/master/LICENSE) | **obfuscated** source code* |
| [paru](https://github.com/Morganamilo/paru) | holo | [GPLv3](https://github.com/Morganamilo/paru/blob/master/LICENSE) | **obfuscated** source code* |
| [pipewire](https://gitlab.freedesktop.org/pipewire/pipewire) | holo | [GPLv2](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/LICENSE), [LGPL](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/LICENSE) | **obfuscated** source code* |
| [plasma-nm](https://invent.kde.org/plasma/plasma-nm) | jupiter | [GPLv3](https://invent.kde.org/plasma/plasma-nm/-/blob/master/LICENSES/GPL-3.0-only.txt), [GPLv2](https://invent.kde.org/plasma/plasma-nm/-/blob/master/LICENSES/GPL-2.0-or-later.txt), [LGPLv3](https://invent.kde.org/plasma/plasma-nm/-/blob/master/LICENSES/LGPL-3.0-only.txt), [LGPLv2.1](https://invent.kde.org/plasma/plasma-nm/-/blob/master/LICENSES/LGPL-2.1-only.txt), [LGPLv2](https://invent.kde.org/plasma/plasma-nm/-/blob/master/LICENSES/LGPL-2.0-or-later.txt) | **obfuscated** source code* |
| [plasma-remotecontrollers](https://invent.kde.org/plasma-bigscreen/plasma-remotecontrollers) | jupiter | [GPLv3](https://invent.kde.org/plasma-bigscreen/plasma-remotecontrollers/-/blob/master/LICENSES/GPL-3.0-only.txt), [GPLv2](https://invent.kde.org/plasma-bigscreen/plasma-remotecontrollers/-/blob/master/LICENSES/GPL-2.0-only.txt) | **obfuscated** source code* |
| [plasma-workspace](https://invent.kde.org/plasma/plasma-workspace) | jupiter | [GPLv3](https://invent.kde.org/plasma/plasma-workspace/-/blob/master/LICENSES/GPL-3.0-only.txt), [GPLv2](https://invent.kde.org/plasma/plasma-workspace/-/blob/master/LICENSES/GPL-2.0-or-later.txt), [LGPLv3](https://invent.kde.org/plasma/plasma-workspace/-/blob/master/LICENSES/LGPL-3.0-or-later.txt), [LGPLv2.1](https://invent.kde.org/plasma/plasma-workspace/-/blob/master/LICENSES/LGPL-2.1-or-later.txt), [LGPLv2](https://invent.kde.org/plasma/plasma-workspace/-/blob/master/LICENSES/LGPL-2.0-or-later.txt) | **obfuscated** source code* |
| [plymouth](https://gitlab.freedesktop.org/plymouth/plymouth) | holo | [GPLv2](https://gitlab.freedesktop.org/plymouth/plymouth/-/blob/main/COPYING) | **obfuscated** source code* |
| [pyzy](https://github.com/pyzy/pyzy) | holo | [LGPLv2.1](https://github.com/pyzy/pyzy/blob/master/COPYING) | **obfuscated** source code* |
| [rauc](https://github.com/rauc/rauc) | holo | [LGPLv2.1](https://github.com/rauc/rauc/blob/master/COPYING) | **obfuscated** source code* |
| [rtl88x2ce-dkms](https://gitlab.com/evlaV/jupiter-PKGBUILD/-/tree/master/rtl88x2ce-dkms) | jupiter | GPLv? | **obfuscated** source code* |
| [sddm-wayland](https://github.com/sddm/sddm) | jupiter | [GPLv2](https://github.com/sddm/sddm/blob/develop/LICENSE) | **obfuscated** source code* |
| [steamos-efi](https://gitlab.com/evlaV/steamos-efi) (fork of gnu-efi) | holo, jupiter | [GPLv2](https://gitlab.com/evlaV/steamos-efi/-/blob/master/COPYING) | **obfuscated** source code* |
| [udisks2](https://github.com/storaged-project/udisks) | jupiter | [GPLv2](https://github.com/storaged-project/udisks/blob/master/COPYING), [LGPLv2](https://github.com/storaged-project/udisks/blob/master/COPYING) | **obfuscated** source code* |
| [upower](https://gitlab.freedesktop.org/upower/upower) | jupiter | [GPLv2](https://gitlab.freedesktop.org/upower/upower/-/blob/master/COPYING) | **obfuscated** source code* |
| [vkmark](https://github.com/vkmark/vkmark) | holo | [LGPLv2.1](https://github.com/vkmark/vkmark/blob/master/COPYING-LGPL2.1) | **obfuscated** source code* |
| [xdg-desktop-portal](https://github.com/flatpak/xdg-desktop-portal) | jupiter | [LGPLv2.1](https://github.com/flatpak/xdg-desktop-portal/blob/main/COPYING) | **obfuscated** source code* |
| [xone-dkms](https://github.com/medusalix/xone) | holo | [GPLv2](https://github.com/medusalix/xone/blob/master/LICENSE) | **obfuscated** source code* |
| [xow](https://github.com/medusalix/xow) | holo | [GPLv2](https://github.com/medusalix/xow/blob/master/LICENSE) | **obfuscated** source code* |
| [zenity-light](https://gitlab.gnome.org/GNOME/zenity) | jupiter | [LGPLv2.1](https://gitlab.gnome.org/GNOME/zenity/-/blob/master/COPYING) | **obfuscated** source code* |

### First/Second Party (Valve, Collabora, and Igalia) Packages/Repositories in Violation of GNU License

| Package/Repository | Branch | License | Reason |
| :--- | :---: | ---: | :---: |
| [kdump-steamos](https://gitlab.com/evlaV/kdump-steamos) | holo | [LGPLv2.1](https://gitlab.com/evlaV/kdump-steamos/-/blob/main/README.md) | **obfuscated** source code* |
| [steamdeck-kde-presets](https://gitlab.com/evlaV/steamdeck-kde-presets) | jupiter | [GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt) | **obfuscated** source code* |
| [steamos-atomupd-client](https://gitlab.com/evlaV/steamos-atomupd) | holo | [LGPLv2](https://gitlab.com/evlaV/steamos-atomupd/-/blob/master/COPYING) | **obfuscated** source code* |
| [steamos-customizations](https://gitlab.com/evlaV/steamos-customizations) | holo | [LGPLv2.1](https://gitlab.com/evlaV/steamos-customizations/-/blob/master/COPYING.LGPL-2.1) | **obfuscated** source code* |
| [steamos-customizations-jupiter](https://gitlab.com/evlaV/steamos-customizations) | jupiter | [LGPLv2.1](https://gitlab.com/evlaV/steamos-customizations/-/blob/master/COPYING.LGPL-2.1) | **obfuscated** source code* |
| [steamos-media-creation](https://gitlab.com/evlaV/steamos-media-creation) | holo | [LGPLv2.1](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt) | **obfuscated** source code* |
| [steamos-netrc](https://gitlab.com/evlaV/holo-PKGBUILD/-/tree/master/steamos-netrc) | holo | GPLv? | **obfuscated** source code* |
| [steamos-repair-backend](https://gitlab.com/evlaV/steamos-repair-backend) | holo | [GPLv2](https://gitlab.com/evlaV/steamos-repair-backend/-/blob/main/COPYING) | **obfuscated** source code* |
| [steamos-repair-tool](https://gitlab.com/evlaV/steamos-repair-tool) | holo | [GPLv2](https://gitlab.com/evlaV/steamos-repair-tool/-/blob/master/COPYING) | **obfuscated** source code* |

***obfuscated** by one or both of the following:

1. ~~**no (written) offer for source code** - no official documentation or link(s) to source code (i.e., officially undocumented) - e.g., [Oracle](https://www.oracle.com/downloads/opensource/software-components-source-code.html) and [Sony PS5](https://doc.dl.playstation.net/doc/ps5-oss/index.html)~~ **(September 21, 2023)**
2. source code requires (officially undocumented) user alteration/modification/deobfuscation to be accessible/legible/usable/buildable (i.e., obfuscated) - see: [Manually Deobfuscate Source Package](#manually-deobfuscate-source-package)
3. official (unmodified) PKGBUILD sources from / depends on [Valve's official private GitLab repositories](https://gitlab.steamos.cloud/) - PKGBUILD provided here are modified to source from / depend on [these unofficial public mirrors](https://gitlab.com/evlaV)

[**These public mirrors (@gitlab.com/evlaV)**](https://gitlab.com/users/evlaV/projects) aim to (unofficially) correct these violations.

***missing** source code:

- see: [GPL Package Research](#gpl-package-research)

---

**(September 21, 2023):** The following notice/offer was (finally) added [574 days after Steam Deck release](https://www.timeanddate.com/date/durationresult.html?m1=2&d1=25&y1=2022&m2=9&d2=21&y2=2023&ti=on) to SteamOS (**Steam Version 1695334486**):

> **This was done purely in response to some back-and-forth between the [Software Freedom Conservancy](https://sfconservancy.org/about/) and [Valve's general/chief counsel (CLO) Liam Lavery](mailto:liaml@valvesoftware.com)**

<img alt="SteamOS Notice" src="images/steamos-notice-1.jpg" width="400">

<img alt="SteamOS Notice" src="images/steamos-notice-2.jpg" width="400">

> *Excuse me, I'm a bit of a stickler [Meeseeks], but **Valve, go home, you're drunk**; your operating system is [officially titled](https://repo.steampowered.com/steamos/README.txt) **SteamOS**, not **Steam OS***

---

### Manually Deobfuscate Source Package

| Path | Description |
| :--- | :--- |
| `$pkgname_dir_1`/ | Main package directory |
| \|__.SRCINFO | Arch Linux AUR package metadata |
| \|__PKGBUILD | Arch Linux package |
| \|__**`$pkgname_dir_2`/** | **Obfuscated package git** (directory name (`$pkgname_dir_2`) may be **identical to** or **differ from** (i.e., a derivative of) **main package directory** name (`$pkgname_dir_1`)) |

```sh
# (at your discretion) proceed to download and extract respective source package to current working directory ($PWD) (e.g., zenity-light)
curl https://steamdeck-packages.steamos.cloud/archlinux-mirror/sources/jupiter-main/zenity-light-3.32.0%2B55%2Bgd7bedff6-1.src.tar.gz | bsdtar -xvf-

# respective package directory name(s) (e.g., zenity-light and zenity)
pkgname_dir_1=zenity-light
pkgname_dir_2=zenity

# deobfuscate
cd "$pkgname_dir_1/$pkgname_dir_2" && \
mkdir -p ".git/" && \
mv * ".git/" && \
git init && \
git checkout -f
```

---

### GPL Package Research

[Latest Steam Deck (jupiter) Recovery Image ("SteamOS Deck Image")](https://store.steampowered.com/steamos/download/?ver=steamdeck)

[Steam Deck (jupiter) Recovery Download Directory](https://steamdeck-images.steamos.cloud/recovery/)

---

**(February 28, 2022):** There were [542 GPL packages of 871 total packages (62.227%)](research/steam-deck-recovery-1-package-info.txt) distributed in [Steam Deck Recovery Image 1](https://steamdeck-images.steamos.cloud/recovery/steamdeck-recovery-1.img.bz2) ("SteamOS Deck Image") ([zip](https://steamdeck-images.steamos.cloud/recovery/steamdeck-recovery-1.img.zip))

> b2sum: `30453a4edab260d6a7af7c74b6287f6361e86d19e4d6d3b45a14c620d0bf2540ac3f281b5947e9541844db41c79681b18156e7db96a9ce95a23e52ba6409e2c9 steamdeck-recovery-1.img.bz2`

---

**(May 15, 2022):** There were [543 GPL packages of 871 total packages (62.342%)](research/steam-deck-recovery-4-package-info.txt) distributed in [Steam Deck Recovery Image 4](https://steamdeck-images.steamos.cloud/recovery/steamdeck-recovery-4.img.bz2) ("SteamOS Deck Image") ([zip](https://steamdeck-images.steamos.cloud/recovery/steamdeck-recovery-4.img.zip))

> b2sum: `ab806fb2374db4135b1a706425c81d367aeeb8508b4417ba652438e52772b88462f029a257f193d6827df8e526bbad9b10c532ef1a437b7393312e5fd4ad9c0e steamdeck-recovery-4.img.bz2`

---

**(as of June 17, 2023):** There were [593 GPL packages of 944 total packages (62.817%)](research/steam-deck-jupiter-main-package-info-20230617.txt) distributed in Steam Deck (jupiter) main (jupiter-main)

**(as of September 21, 2023):** There were [595 GPL packages of 971 total packages (61.277%)](research/steam-deck-jupiter-main-package-info.txt) distributed in Steam Deck (jupiter) main (jupiter-main)

---

*Thank you [**Valve**](https://www.valvesoftware.com/en/about), [**Collabora**](https://www.collabora.com/about-us/), and [**Igalia**](https://www.igalia.com/about/)*; *I know how truly **difficult** and **lengthy/time-consuming** GitLab repository publication can be*; [the following how-to](#-gitlab-repository-private-public-how-to-) was made specifically for you:

## 😉 GitLab Repository Private-Public How-to 😂

The following must be conducted per [GitLab repository](https://gitlab.steamos.cloud/):

1. Left click `Settings` or mouseover `Settings` then left click `General` (`Settings > General`)
2. Expand `Visibility, project features, permissions` (left click `Expand`)
3. Change `Project visibility` dropdown menu option from `Private` to `Public` (left click `Private` then left click `Public`)
4. Left click `Save changes`

Here's a visual example using `jupiter-hw-support`:

<img alt="GitLab Private-Public How-to" src="images/gitlab-private-public-how-to.gif" width="512">

Maybe [**Valve**](https://www.valvesoftware.com/en/about), [**Collabora**](https://www.collabora.com/about-us/), and [**Igalia**](https://www.igalia.com/about/) will perform this *daunting* task in ~~2022~~ 2023(?) 🤞

## 💭 Thoughts/Opinions 💡

Valve has commercialized a product (SteamOS 3.x / Steam Deck) which almost exclusively relies on and utilizes (free/libre) open source software. Software myself and countless others have contributed to for decades. Valve has capitalized/profited immensely from (free/libre) open source software, and in turn has not been very friendly to the (free/libre) open source software community. This is why (in protest) I set up these public mirrors on April 1, 2022. **I chose April Fools' Day as a nod to the foolishness of Valve regarding publication.**

Valve likes to distance themselves from proprietary technology companies (e.g., Microsoft Windows) but Valve SteamOS isn't much different; SteamOS is a privately developed OS, with proprietary software and DRM (Steam) front and center, consisting of mostly public sources ([the majority of which are GPL](#gpl-package-research)). The Steam Deck additionally contains proprietary hardware, which includes additional proprietary software (this is also true for the [Steam Deck Docking Station](https://www.steamdeck.com/en/dock)). This can thankfully be (mostly) alleviated/liberated by [OpenSD](https://gitlab.com/open-sd/opensd) (thank you [@seek-dev](https://gitlab.com/seek-dev)).

Paired with Valve's deliberate privatization of their GitLab repositories and poor interactions regarding publication (over **18 months** later) makes it comparatively easy to recommend the [ASUS ROG Ally](https://rog.asus.com/gaming-handhelds/rog-ally/rog-ally-2023/) (June 13, 2023). Who doesn't love a global launch? Moreover, the similar nature of [publication date](https://www.theverge.com/2023/4/1/23666084/asus-rog-ally-handheld-windows-gaming-portable) (April Fools' Day). And like the Steam Deck, you're free to install **YOUR CHOICE of OS** (or use pre-installed Windows 11). Microsoft Windows and Valve SteamOS are pretty comparable to me, though I'd argue Windows 11 behaves as expected outside of gaming (desktop use). SteamOS 3.x / Steam Deck does not behave or perform anything like Arch / GNU/Linux. It's tragic knowing some people's first/early interactions with GNU/Linux will be via SteamOS. What a terrible/inaccurate representation of GNU/Linux (to say the least). Regardless, I'd recommend the same for both (Steam Deck and ROG Ally): take advantage of the hardware and install something (anything) else.

I'd advocate the openness of Valve and the Steam Deck, but I unfortunately cannot attest to that. SteamOS 3.x is a terrible (and I honestly think worthy of the title "worst") distribution of Linux (e.g., SteamOS 3.x doesn't comply with [Arch Linux](https://archlinux.org/about/) or [GNU/Linux](https://www.gnu.org/philosophy/philosophy.html) philosophies), with Steam (proprietary and [DRM](https://en.wikipedia.org/wiki/Digital_rights_management)) front and center, all else is secondary (or nonexistent); this unfortunately includes the (free/libre) open source software community, who are the very foundation which SteamOS is built upon. Look at the polarity between SteamOS 3.x / Steam Deck Gaming mode versus Desktop (and Recovery) mode to see where Valve's focus lies. The majority of Steam Deck updates enhance Steam / Gaming mode. 'Switch to Desktop' is the bottom/last Power option (excluding 'Cancel'). 'Return to Gaming Mode' (the mode the system (always) boots to) is the first (primary) Desktop option. SteamOS 3.x / Steam Deck is buttery smooth and pleasant in Steam / Gaming mode, compared to rough/painful (to say the least) in Desktop mode.

To say a bit more, SteamOS is rather easy to [soft brick](https://en.wikipedia.org/wiki/Brick_(electronics)#Soft_brick) (I've soft bricked SteamOS more times than I'd like to admit). SteamOS (jupiter) lacks a C/C++ compiler (e.g., GCC / glibc or Clang), doesn't have a very expansive/functional instance of Python (e.g., no Python package / pip support), can't traditionally install packages/software or retain modifications, and has a wonky bootloader (e.g., dual booting). Dual booting can beautifully be resolved by [SteamDeck-Clover-Dualboot](https://github.com/ryanrudolfoba/SteamDeck-Clover-Dualboot) (thank you [@ryanrudolfoba](https://github.com/ryanrudolfoba)).

It's apparent Valve doesn't expect (or prioritize) the use of Desktop mode. A huge and completely missed opportunity for Valve, SteamOS, Steam Deck, GNU/Linux, and the entire community. I think GNOME would be a much better desktop environment, especially for touch screen devices like the Steam Deck (or any portable for that matter), but I believe Valve chose KDE due to their [special (financial) interests](https://www.phoronix.com/news/Valve-Funding-KWin-Work). So the Steam Deck and the community suffer only to "justify" Valve's investments in KDE development. Money talks. Why not embrace the many nuances of Arch / GNU/Linux (and its users) and enable the use of any (Arch supported) desktop environment?

Many of Valve's commercial/hardware projects/products fail (e.g., SteamOS 1 and 2, Steam Link, and Steam Controller); the truly sad thing is Valve's ideas are generally good (on paper), but (as Valve has repeatedly demonstrated) are extremely poorly managed and executed by uncoordinated, unmotivated, ignorant, incompetent, and **unethical** people. I believe this is a side effect of Valve's ['flat hierarchy' facade](https://www.theguardian.com/commentisfree/2018/jul/30/no-bosses-managers-flat-hierachy-workplace-tech-hollywood). I predict SteamOS 3.x will have many of the same issues (and more) SteamOS 1 and 2 had. This is further compounded by Valve following/projecting a similar canonical/conventional (milestone) ideology (from Debian) to Arch. **This concept/implementation is deeply flawed, incompatible, and conflicts with Arch's bleeding edge rolling release! Asinine!**

**SteamOS is technically Linux; oriented to directly benefit Valve (primarily/solely promoting itself (Steam) and DRM); Valve is very much like any other corporation who markets proprietary/non-free/closed-source software/OS (e.g., Apple macOS and Microsoft Windows). SteamOS possesses none of the [philosophy of GNU](https://www.gnu.org/philosophy/philosophy.html). And deserves a proper (up-to-date) [GNU unendorsement](https://www.gnu.org/distros/common-distros.html).**

Valve clearly aren't interested in public access to their code or contributions. I'm honestly questioning if Valve will ever publicize their GitLab repositories or open SteamOS development to the public (contributions and issues). I'll likely be maintaining these public mirrors for the foreseeable future...

This is not ideal, as I (an unaffiliated third party) should not be trusted to host Valve's entire SteamOS 3.x / Steam Deck [codebase](https://en.wikipedia.org/wiki/Codebase). I haven't made any alterations, except (when necessary) pointing to [these unofficial public mirrors](https://gitlab.com/evlaV) instead of [Valve's official private GitLab repositories](https://gitlab.steamos.cloud/). Users of these unofficial public mirrors have to trust me, and there isn't an easy or intuitive way to verify the code on these public mirrors hasn't been tampered with (because Valve's official GitLab repositories are still private). With skepticism, I hope these public mirrors serve as the de facto place to source, verify, and follow official SteamOS 3.x / Steam Deck software/updates.

I've graciously done Valve's ("last mile") source code distribution job/work duty (free of charge) for over **18 months** now, and will continue to do so for the foreseeable future, hopefully filling the void left from Valve. Through this void filling effort, I've interacted with several developers of projects who have benefited from these public mirrors. From individual/add-on projects like [SteamOS Btrfs](https://gitlab.com/popsulfr/steamos-btrfs) (thank you [@popsulfr (Philipp Richter)](https://gitlab.com/popsulfr)) to entire replacement Linux distributions like [batocera](https://batocera.org/), [bazzite](https://github.com/ublue-os/bazzite), and [ChimeraOS](https://chimeraos.org/). That's precisely the purpose of this project; to aid, encourage, and inspire SteamOS 3.x / Steam Deck contributors/developers/tinkerers. If you have any questions or comments, [write to me](mailto:valve.evlav@proton.me).

---

*"The truly intelligent person is one who can pretend to be a fool in front of a fool who pretends to be intelligent."* ―

---

*"The best life is the one in which the creative impulses play the largest part and the possessive impulses the smallest."* ― **Bertrand Russell, Pacifism and Revolution**

---

*"I should like to say two things, one intellectual and one moral:*

*The intellectual thing I should want to say to them is this: When you are studying any matter or considering any philosophy, ask yourself only what are the facts and what is the truth that the facts bear out. Never let yourself be diverted either by what you wish to believe or by what you think would have beneficent social effects if it were believed, but look only and solely at what are the facts. That is the intellectual thing that I should wish to say.*

*The moral thing I should wish to say to them is very simple. I should say: **Love is wise, hatred is foolish. In this world, which is getting more and more closely interconnected, we have to learn to tolerate each other. We have to learn to put up with the fact that some people say things that we don’t like. We can only live together in that way, and if we are to live together and not die together we must learn a kind of charity and a kind of tolerance which is absolutely vital to the continuation of human life on this planet.**"* ― **Bertrand Russell, [BBC Face to Face (1959)](https://www.youtube.com/watch?v=a10A5PneXlo&t=1646s)**

---

*"Education can help us only if it produces “whole men”. The truly educated man is not a man who knows a bit of everything, not even the man who knows all the details of all subjects (if such a thing were possible): the “whole man” in fact, may have little detailed knowledge of facts and theories, he may treasure the Encyclopædia Britannica because “she knows and he needn’t”, but he will be truly in touch with the centre. He will not be in doubt about his basic convictions, about his view on the meaning and purpose of his life. He may not be able to explain these matters in words, but the conduct of his life will show a certain sureness of touch which stems from this inner clarity."* ― **E. F. Schumacher, Small is Beautiful: A Study of Economics as if People Mattered**

---

*"People who talk well but do nothing are like musical instruments; the sound is all they have to offer."* ― **Diogenes, Herakleitos and Diogenes**

---

*"Missing from such histories are the countless small actions of unknown people that led up to those great moments. When we understand this, we can see that the tiniest acts of protest in which we engage may become the invisible roots of social change."* ― **Howard Zinn, You Can't Be Neutral on a Moving Train: A Personal History of Our Times**

---

*"The best people possess a feeling for beauty, the courage to take risks, the discipline to tell the truth, the capacity for sacrifice. Ironically, their virtues make them vulnerable; they are often wounded, sometimes destroyed."* ― **Ernest Hemingway, The Letters of Ernest Hemingway**

---

*“Without music, life would be a mistake.”* ― **Friedrich Nietzsche, Twilight of the Idols**

---

🎵 *I'm the voice inside your head, you refuse to hear. I'm the face that you have to face, mirroring your stare. I'm what's left, I'm what's right, I'm the enemy. I'm the hand that'll take you down, bring you to your knees. So, who are you?* 🎵

🎵 ***Keep you in the dark, you know they all pretend.*** 🎵

---

*"If you scratch a cynic, you'll find a disappointed idealist."* ― **George Carlin**

---

*"Whoever fights with monsters should see to it that he does not become one himself. And when you stare for a long time into an abyss, the abyss stares back into you."* ― **Friedrich Nietzsche, Beyond Good and Evil**

---

**(May 28, 2023):** The following [free speech flags](https://en.wikipedia.org/wiki/Free_Speech_Flag) belong here because [~~reasons~~](https://dolphin-emu.org/blog/6T/) [psychological reactance](https://en.wikipedia.org/wiki/Reactance_(psychology)) / [Streisand effect](https://en.wikipedia.org/wiki/Streisand_effect) | [update](https://www.theverge.com/2023/6/1/23745772/valve-nintendo-dolphin-emulator-steam-emails) | [aftermath](https://dolphin-emu.org/blog/6U/):

<img alt="Free Speech Flag (Wii)" src="images/free-speech-flag-wii.png" width="480">

<img alt="Free Speech Flag (Wii U)" src="images/free-speech-flag-wii-u.png" width="480">

- Wii Common Key: **EBE42A225E8593E448D9C5457381AAF7** | [bin](keys/wii-common-key.bin) | [txt](keys/wii-common-key.txt)
- Wii U Common Key: **D7B00402659BA2ABD2CB0DB27FA2B656** | [bin](keys/wii-u-common-key.bin) | [txt](keys/wii-u-common-key.txt)

## 📜 License 📜

The contents of [this project (jupiter-PKGBUILD)](https://gitlab.com/evlaV/jupiter-PKGBUILD) are licensed under the [Mozilla Public License Version 2.0 (MPL 2.0)](LICENSE), and the underlying packages are licensed under their respective license(s) (see: respective package repository, directory, and/or PKGBUILD).

The following contents are [free, unencumbered, and released into the public domain](UNLICENSE) or (equivalently) licensed under the [BSD Zero Clause License (0-clause)](LICENSE.0BSD):

- [`images/free-speech-flag-wii.svg`](images/free-speech-flag-wii.svg) ([`images/free-speech-flag-wii.png`](images/free-speech-flag-wii.png))
- [`images/free-speech-flag-wii-u.svg`](images/free-speech-flag-wii-u.svg) ([`images/free-speech-flag-wii-u.png`](images/free-speech-flag-wii-u.png))
